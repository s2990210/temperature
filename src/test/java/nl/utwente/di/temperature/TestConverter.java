package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Converter
 */
public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double price = converter.getFahrenheit("1");
        Assertions.assertEquals(33.8, price, 0.0, "1C = 33.8F");
    }
}
